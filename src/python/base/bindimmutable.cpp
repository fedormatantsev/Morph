#include "base/immutable/map.h"

#include <boost/python.hpp>

using namespace boost::python;

namespace
{

struct BindHash
{
    std::size_t operator()(const object& value)
    {
        return PyObject_Hash(value.ptr());
    }
};

}

BOOST_PYTHON_MODULE(_morph)
{

using Map = immutable::Map<object, object, BindHash>;
class_<Map>("Map")
    .def("__setitem__", &Map::set)
    .def("__getitem__", &Map::get, return_value_policy<copy_const_reference>())
    .def("__delitem__", &Map::erase);
}
